'''
Maneja un diccionario con los artículos de una tienda y
sus precios, y permite luego comprar alguno de sus elementos,
y ver el precio total de la compra.
'''
import sys

articulos = {}

def anadir(articulo, precio):
    """Añade un artículo y un precio al diccionario artículos"""
    articulos[articulo] = precio
def mostrar():
    """Muestra en pantalla todos los artículos y sus precios"""
    print("lista de articulos en la tienda: ")
    for articulo, precio in articulos.items():
        print(f"- {articulo}: {precio} euro")
def pedir_articulo() -> str:
    """Pide al usuario el artículo a comprar.

    Muestra un mensaje pidiendo el artículo a comprar.
    Cuando el usuario lo escribe, comprueba que es un artículo qie
    está en el diccionario artículos, y termina devolviendo ese
    artículo como string. Si el artículo escrito por el usuario no
    está en artículos, pide otro hasta que escriba uno que sí está."""

    articulo = input("articulo: ")
    while True:
        if articulo not in articulos:
            print()
        else:
            break

    return articulo

def pedir_cantidad() -> float:
    """Pide al usuario la cantidad a comprar.

    Muestra un mensaje pidiendo la cantidad a comprar.
    Cuando el usuario lo escribe, comprueba que es un número real
    (float), y termina devolviendo esa cantidad.. Si la cantidad
    escrita no es un float, pide otra hasta que escriba una correcta.
    """

    while True:
        try:
            cantidad = float(input("cantidad: "))

            return cantidad

        except ValueError:
            print("cantidad: ")


def main():
    """Toma los artículos declarados como argumentos en la línea de comando,
    junto con sus precios, almacénalos en el diccionario artículos mediante
    llamadas a la función añadir. Luego muestra el listado de artículos
    y precios para que el usuario pueda elegir. Termina llamando a las
    funciones pedir_articulo y pedir cantidad, y mostrando en pantalla la
    cantidad comprada, de qué artículo, y cuánto es su precio.
    """

    for i in range(1, len(sys.argv), 2):
        articulo = sys.argv[i]
        precio = float(sys.argv[i + 1])
        anadir(articulo, precio)

    mostrar()

    articulo = pedir_articulo()
    cantidad = pedir_cantidad()

    print(f"compra total : {cantidad} de {articulo}, a pagar {articulos[articulo] * cantidad}")


if __name__ == '__main__':
    main()